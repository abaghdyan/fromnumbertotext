﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromNumberToText
{
    class Program
    {
        static void Main(string[] args)
        {
            var endDictionary = new Dictionary<int, string>();
            NumberToText.AddDictionary(endDictionary);

            while (true)
            {
                Console.WriteLine("Please enter the number.\nFor exit please enter \"Exit\".");
                Console.ForegroundColor = ConsoleColor.Green;
                string number = Console.ReadLine();

                if (number == "Exit")
                {
                    return;
                }

                int.TryParse(number, out int result);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(NumberToText.GetTextFromNumber(result, endDictionary));
                Console.ResetColor();
            }
        }
    }
}
