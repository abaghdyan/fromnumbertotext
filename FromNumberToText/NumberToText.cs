﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromNumberToText
{
    static class NumberToText
    {
        public static string GetTextFromNumber(int number, Dictionary<int, string> engDictionary)
        {
            if (number == 0)
                return engDictionary[0];

            if (number < 0)
                return "minus " + GetTextFromNumber(Math.Abs(number), engDictionary);

            string textNumber = "";

            if ((number / 1000000) > 0)
            {
                textNumber += GetTextFromNumber(number / 1000000, engDictionary) + engDictionary[1000000] + " ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                textNumber += GetTextFromNumber(number / 1000, engDictionary) + engDictionary[1000] + " ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                textNumber += GetTextFromNumber(number / 100, engDictionary) + engDictionary[100] + " ";
                number %= 100;
            }

            if (number > 0)
            {
                if (number < 20)
                    textNumber += engDictionary[number] + " ";
                else
                {
                    textNumber += engDictionary[number - number % 10] + " ";
                    if ((number % 10) > 0)
                        textNumber += engDictionary[number % 10] + " ";
                }
            }

            return textNumber;
        }

        public static void AddDictionary(Dictionary<int, string> engDictionary)
        {
            engDictionary.Add(0, "zero");
            engDictionary.Add(1, "one");
            engDictionary.Add(2, "two");
            engDictionary.Add(3, "three");
            engDictionary.Add(4, "four");
            engDictionary.Add(5, "five");
            engDictionary.Add(6, "six");
            engDictionary.Add(7, "seven");
            engDictionary.Add(8, "eight");
            engDictionary.Add(9, "nine");
            engDictionary.Add(10, "ten");
            engDictionary.Add(11, "eleven");
            engDictionary.Add(12, "twelve");
            engDictionary.Add(13, "thirteen");
            engDictionary.Add(14, "fourteen");
            engDictionary.Add(15, "fifteen");
            engDictionary.Add(16, "sixteen");
            engDictionary.Add(17, "seventeen");
            engDictionary.Add(18, "eighteen");
            engDictionary.Add(19, "nineteen");
            engDictionary.Add(20, "twenty");
            engDictionary.Add(30, "thirty");
            engDictionary.Add(40, "forty");
            engDictionary.Add(50, "fifty");
            engDictionary.Add(60, "sixty");
            engDictionary.Add(70, "seventy");
            engDictionary.Add(80, "eighty");
            engDictionary.Add(90, "ninety");
            engDictionary.Add(100, "hundred");
            engDictionary.Add(1000, "thousand");
            engDictionary.Add(1000000, "milion");
        }
    }
}
